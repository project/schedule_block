<?php

namespace Drupal\schedule_block\Plugin\Condition;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Expire Block' condition.
 *
 * @Condition(
 *   id = "expire_block",
 *   label = @Translation("Expire on")
 * )
 */
class ExpireBlockCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array              $configuration,
                       $plugin_id,
                       $plugin_definition
  ) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->setTime($container->get('datetime.time'));
    return $instance;
  }

  /**
   * Setter for the time service.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   */
  protected function setTime(TimeInterface $time) {
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    return t('Un-publish block when date & time has expired.');
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $default_end = (!empty($this->configuration['end'])) ? DrupalDateTime::createFromTimestamp($this->configuration['end']) : '';
    $form['end'] = [
      '#type' => 'datetime',
      '#title' => t('Expiry Date'),
      '#default_value' => $default_end,
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (is_object($form_state->getValue('end'))) {
      $this->configuration['end'] = $form_state->getValue('end')
        ->getTimestamp();
    }
    else {
      $this->configuration['end'] = '';
    }
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $status = TRUE;
    if (empty($this->configuration['end']) && !$this->isNegated()) {
      return TRUE;
    }
    if (!empty($this->configuration['end'])) {
      $status = $status && time() <= $this->configuration['end'];
    }
    return $status;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['end' => ''] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    $current_time = $this->time->getRequestTime();
    $max_age = Cache::PERMANENT;
    if ((int) $this->configuration['end'] > $current_time) {
      // If the unpublished time is in the future, use that.
      $max_age = $this->configuration['end'] - $current_time;
    }
    return Cache::mergeMaxAges(parent::getCacheMaxAge(), $max_age);
  }
}
