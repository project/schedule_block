<?php
namespace Drupal\schedule_block\Plugin\Condition;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Publish Block' condition.
 *
 * @Condition(
 *   id = "publish_block",
 *   label = @Translation("Publish on")
 * )
 */
class PublishBlockCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array              $configuration,
                       $plugin_id,
                       $plugin_definition
  ) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->setTime($container->get('datetime.time'));
    return $instance;
  }

  /**
   * Setter for the time service.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   */
  protected function setTime(TimeInterface $time) {
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    return t('Publish block on specified date & time.');
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $default_start = (!empty($this->configuration['start'])) ? DrupalDateTime::createFromTimestamp($this->configuration['start']) : '';
    $form['start'] = [
      '#type' => 'datetime',
      '#title' => t('Publish Date'),
      '#default_value' => $default_start,
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (is_object($form_state->getValue('start'))) {
      $this->configuration['start'] = $form_state->getValue('start')
        ->getTimestamp();
    }
    else {
      $this->configuration['start'] = '';
    }
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $status = TRUE;
    if (empty($this->configuration['start']) && !$this->isNegated()) {
      return TRUE;
    }
    if (!empty($this->configuration['start'])) {
      $status = $status && time() >= $this->configuration['start'];
    }
    return $status;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['start' => ''] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    $current_time = $this->time->getRequestTime();
    $max_age = Cache::PERMANENT;
    if ($this->configuration['start'] > $current_time) {
      $max_age = $this->configuration['start'] - $current_time;
    }
    return Cache::mergeMaxAges(parent::getCacheMaxAge(), $max_age);
  }
}
