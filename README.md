# Schedule Block

Module provides two conditions to publish and expire blocks on a specified date & time.

## Usage

Adds two new options under block visibility for showing and hiding block.

Setup either condition or both if you would like.

## Installation

Install module just like any other contrib module.

The only configuration that is needed is on the block configuration page.
